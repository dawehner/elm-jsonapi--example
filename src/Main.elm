module Main exposing (main)

import Html exposing (button, div, li, text, ul)
import Html.Events exposing (onClick)
import Http
import Json.Decode
import JsonApi
import JsonApi.Http
import JsonApi.Resources
import Result.Extra


main =
    Html.program
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }



--- Model


type alias Package =
    { name : String
    , summary : String
    , versions : List String
    }


type alias Model =
    { packages : Result String (List Package)
    , package : Result String Package
    }


type Msg
    = GetPackages
    | LoadPackagesResult (Result String (List Package))
    | GetPackage
    | LoadPackageResult (Result String Package)


init : ( Model, Cmd Msg )
init =
    ( { packages = Result.Err "Packages: Not loaded yet", package = Result.Err "Package: Not loaded yet" }, Cmd.none )


decodePackage : Json.Decode.Decoder Package
decodePackage =
    Json.Decode.map3 Package
        (Json.Decode.field "name" Json.Decode.string)
        (Json.Decode.field "summary" Json.Decode.string)
        (Json.Decode.field "versions" (Json.Decode.list Json.Decode.string))


getPackage : Cmd Msg
getPackage =
    JsonApi.Http.getPrimaryResource "http://localhost:4123/package/FE09D611-F52F-46B4-8AF5-7F35631FF4B9"
        |> Http.send decodePackageResult
        |> Cmd.map LoadPackageResult


decodePackageResult : Result Http.Error JsonApi.Resource -> Result String Package
decodePackageResult result =
    case result of
        Ok resource ->
            JsonApi.Resources.attributes decodePackage resource

        Err error ->
            Err "Loading failed"


getPackages : Cmd Msg
getPackages =
    JsonApi.Http.getPrimaryResourceCollection "http://localhost:4123/package"
        |> Http.send decodePackagesResult
        |> Cmd.map LoadPackagesResult


decodePackagesResult : Result Http.Error (List JsonApi.Resource) -> Result String (List Package)
decodePackagesResult result =
    case result of
        Ok resources ->
            List.map (JsonApi.Resources.attributes decodePackage) resources
                |> Result.Extra.combine

        Err error ->
            Err "Loading failed"



--- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetPackages ->
            ( model, getPackages )

        LoadPackagesResult result ->
            ( { model | packages = result }, Cmd.none )

        GetPackage ->
            ( model, getPackage )

        LoadPackageResult result ->
            ( { model | package = result }, Cmd.none )



--- View


view : Model -> Html.Html Msg
view model =
    div []
        [ button [ onClick GetPackage ] [ text "Get package" ]
        , button [ onClick GetPackages ] [ text "Get packages" ]
        , case model.packages of
            Ok packages ->
                List.map (\package -> li [] [ text package.name ]) packages
                    |> ul []

            Err error ->
                div [] [ text error ]
        , case model.package of
            Ok package ->
                text package.name

            Err error ->
                div [] [ text error ]
        ]
