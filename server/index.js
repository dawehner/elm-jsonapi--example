const jsonApi = require("jsonapi-server");

jsonApi.setConfig({
	port: 4123,
	swagger: {
    title: "Elm packages",
    version: "0.1.1",
    description: "A JSONAPI server for elm packages",
  },
});

const packages = [
	{
		"id": "FE09D611-F52F-46B4-8AF5-7F35631FF4B9",
    "type": "package",
		"name": "Apanatshka/elm-list-ndet",
		"summary": "Nondeterministic values / cartesian combinable lists",
		"versions": [
			"1.0.1",
			"1.0.0"
		]
	},
	{
    "id": "21C328AF-2A83-4067-8826-A87BCD68794A",
    "type": "package",
		"name": "Apanatshka/elm-signal-extra",
		"summary": "Signal-related, advanced and convenience functions",
		"versions": [
			"1.0.0"
		]
	},
	{
    "id": "0736D9C9-5CE7-4D0B-92EB-3B7A2F06E2A3",
    "type": "package",
		"name": "Bernardoow/elm-alert-timer-message",
		"summary": "Simple message alert library.",
		"versions": [
			"1.0.0"
		]
	}
];

jsonApi.define({
  resource: "package",
  handlers: new jsonApi.MemoryHandler(),
  examples : packages,
  attributes: {
    name: jsonApi.Joi.string(),
    summary: jsonApi.Joi.string(),
    versions: jsonApi.Joi.array().items(jsonApi.Joi.string()),
	},

});

jsonApi.start();
